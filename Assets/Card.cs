﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

	bool flipped = false;
	public static int flipCount = 0;
	static string[] flippedNames = new string[2];

	public float resetTime = 2;
	float resetIn = 0;
	bool pleaseUnflip = false;

	bool pleasePick = false;
	bool picked = false;

	// Use this for initialization
	void Start () {
		resetIn = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (flipCount>=2 && flipped){
			if (name == flippedNames[0] && flippedNames[0]==flippedNames[1]){
				pleasePick = true;
			} else {
				resetIn += Time.deltaTime;
				if (resetIn>resetTime){
					resetIn = 0;
					pleaseUnflip = true;
				}
			}
		} else {
			resetIn = 0;
		}
	}

	void LateUpdate(){
		if (pleasePick){
			pleasePick = false;
			picked = true;
			flipped = false;
			flipCount = 0;
		}
		if (pleaseUnflip){
			pleaseUnflip = false;
			UnFlip();
		}
	}

	void Flip(){
		if (!flipped && !picked){
			flippedNames[flipCount] = name;
			flipped = true;
			flipCount++;
			transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
		}
	}
	void UnFlip(){
		if (flipped && !picked){
			flipped = false;
			flipCount--;
			transform.rotation = Quaternion.identity;
		}
	}

	void OnMouseDown(){
		if (!flipped && flipCount<2 && !picked){
			Flip();
		}
	}

}
