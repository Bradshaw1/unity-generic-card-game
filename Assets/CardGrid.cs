﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGrid : MonoBehaviour {

	public float gridSpacing;
	public int columns;
	public GameObject[] prefabs;
	public Vector3 origin;
	public Vector3 right;
	public Vector3 down;
	List<GameObject> draw;
	// Use this for initialization
	void Start () {
		draw = new List<GameObject>();
		foreach (GameObject go in prefabs) {
			draw.Add(Instantiate(go));
			draw.Add(Instantiate(go));
		}
		draw.Shuffle();
		for (int i = 0; i < draw.Count; i++) {
			draw[i].transform.position =
				origin +
				right * (i % columns) +
				down * (Mathf.Floor(i / columns));
		}
	}
	void Update(){
		for (int i = 0; i < draw.Count; i++) {
			draw[i].transform.position =
				origin +
				right * (i % columns) +
				down * (Mathf.Floor(i / columns));
		}
	}
}

public static class ShuffleExtension {
	public static void Shuffle<T>(this List<T> list) {
		for (int i = 0; i < list.Count; i++) {
			int pick = Random.Range(i, list.Count);
			if (i!=pick){
				T store = list[i];
				list[i] = list[pick];
				list[pick] = store;
			}
		}
	}
}